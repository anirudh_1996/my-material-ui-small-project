import React from 'react';
import Card from '@material-ui/core/card'
import CardAction from '@material-ui/core/CardActions'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { CardContent } from '@material-ui/core';

const course = (props =>{
    return(
        <div>
            {props.course ?(
                <Card>
                    <CardMedia style={{height:0, paddingTop:'56.25%'}}
                    image={props.course.fields.courseImage.fields.file.url}
                    title={props.course.fields.title}></CardMedia>
                    <CardContent>
                        <Typography gutterBottom variant="h5" >
                            {props.course.fields.title}
                        </Typography>
                        <Typography component="p">
                            {props.course.fields.description}
                        </Typography>
                    </CardContent>
                    <CardAction>
                        <Button size="small" color="primary" href={props.course.fields.url} target="_blank"> Go To Course</Button>
                    </CardAction>
                </Card>
            ):null}
        </div>
    )
})
export default course;