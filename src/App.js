import React from 'react';
import './App.css';
import NavBar from './components/Navbar'
import CourseList from './components/CourseList'

function App() {
  return (
  <div>
    <NavBar/>
    <CourseList/>
  </div>
  );
}

export default App;
